R ?= R

%.pdf: %.svg
	inkscape -A $@ $<
	pdfcrop $@
	mv $(dir $@)*-crop.pdf $@

%.png: %.svg
	inkscape -e $@ -d 300 $<

%.tex: %.Rnw
	$(R) CMD Sweave --encoding=utf-8 $<

%.pdf: %.tex $(wildcard *.bib) $(wildcard *.tex)
	latexmk -pdflatex=xelatex -bibtex -use-make -pdf $<
