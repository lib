SNP_INFO ?=$(shell dir=./; while [ "$$( (cd $${dir} >/dev/null; pwd) )" != "/" ]; do if [ -e "$${dir}bin/snp_info" ]; then echo $${dir}bin/snp_info; break; fi; dir="$${dir}../"; done;)

snp_information.txt: snp_list.txt
	if [ -e $@.target.md5sum ] && [ -e $@ ] && md5sum --status --check $@.target.md5sum; then \
		touch $@; \
	else \
	    	$(SNP_INFO) $(SNP_INFO_OPTIONS) $< > $@; \
		md5sum $^ > $@.target.md5sum; \
	fi;
