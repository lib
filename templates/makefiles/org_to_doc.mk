EMACSARGS?=--batch  -l ~/.emacs 
EMACS?=emacs

# The tempdir here is because libreoffice doesn't properly handle two
# instances using the same home directory, and will fail if you're
# exporting with another libreoffice instance open.
$(DOC_TARGETS): %.doc: %.org
	TEMPDIR="$$(mktemp -d)"; \
	SYSUSERCONFIG="file://$$TEMPDIR" $(EMACS) $(EMACSARGS) --visit $^ --eval '(setq org-odt-preferred-output-format "doc")' --funcall org-odt-export-to-odt; \
	rm -rf "$$TEMPDIR";
