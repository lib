GENE_INFO ?=$(shell dir=./; while [ "$$( (cd $${dir} >/dev/null; pwd) )" != "/" ]; do if [ -e "$${dir}bin/gene_info" ]; then echo $${dir}bin/gene_info; break; fi; dir="$${dir}../"; done;)

GENE_INFO_OPTIONS ?= --splicing

gene_information.txt: gene_list.txt $(GENE_INFO) 
	    	$(GENE_INFO) $(GENE_INFO_OPTIONS) $< > $@;
