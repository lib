\documentclass[english,12pt]{article}
\usepackage{fontspec}
\setmainfont{FreeSerif}
\setsansfont{FreeSans}
\setmonofont{FreeMono}
\setmathrm{FreeSerif}
\setmathsf{FreeSans}
\setmathtt{FreeSerif}
\usepackage[letterpaper,left=1in,right=1in,top=1in,bottom=1in,headheight=14pt]{geometry}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage[bf]{caption}
\usepackage{rotating}
\usepackage{wrapfig}
\usepackage{booktabs}
\usepackage{float}
\usepackage{multirow}
\usepackage{setspace}
\usepackage{acro}
\usepackage{array}
\usepackage{dcolumn}
\usepackage{adjustbox}
\usepackage{longtable}
\usepackage{pdflscape}
\usepackage[backend=biber,natbib=true,hyperref=true,style=numeric-comp]{biblatex}
\addbibresource{references.bib}
%\usepackage[noblocks,auth-sc]{authblk}
\usepackage[hyperfigures,bookmarks,colorlinks]{hyperref}
%\usepackage[hyperfigures,bookmarks,colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=black]{hyperref}
% Floats at end for journals
% \usepackage[nolists,figuresfirst,nomarkers,heads,nofighead,notabhead]{endfloat}
% \AtBeginFigures{\section*{Figure Legends}\renewcommand{\efloatseparator}{\hbox{}}}
% \AtBeginTables{\section*{Tables}\renewcommand{\efloatseparator}{\mbox{}}}
\usepackage[nomargin,inline,draft]{fixme}
\newcommand{\DLA}[1]{\textcolor{red}{\fxnote{DLA: #1}}}
\usepackage[x11names,svgnames]{xcolor}
\usepackage{texshade}
\usepackage{tikz}
\usepackage{nameref}
\usepackage{xspace}
% this package will stretch the font slightly if necessary.
\usepackage{microtype}
\usepackage{zref-xr,zref-user}
\usepackage[capitalize]{cleveref}
\def\newblock{\hskip}
\newenvironment{paperquote}{%
  \begin{quote}%
     \it
  }%
  {\end{quote}}
\renewcommand{\textfraction}{0.15}
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{0.65}
\renewcommand{\floatpagefraction}{0.60}
%\renewcommand{\baselinestretch}{1.8}
\newenvironment{enumerate*}%
  {\begin{enumerate}%
    \setlength{\itemsep}{0pt}%
    \setlength{\parskip}{0pt}}%
  {\end{enumerate}}
\newenvironment{itemize*}%
  {\begin{itemize}%
    \setlength{\itemsep}{0pt}%
    \setlength{\parskip}{0pt}}%
  {\end{itemize}}
\oddsidemargin 0.0in 
\textwidth 6in
\raggedbottom
\clubpenalty = 10000
\widowpenalty = 10000
\setlength{\emergencystretch}{20em}
\renewcommand\Affilfont{\itshape\small}
\renewcommand\cite[1]{\autocite{#1}}
\pagestyle{fancy}
\author{AUTHOR}
\title{TITLE}
\date{\ }
\onehalfspacing
% \doublespacing
                 
\begin{document}

<<load.libraries,echo=FALSE,results="hide",warning=FALSE,message=FALSE,error=FALSE,cache=FALSE>>=
opts_chunk$set(dev="cairo_pdf",out.width="\\textwidth",out.height="0.7\\textheight",out.extra="keepaspectratio")
#opts_chunk$set(cache=TRUE, autodep=TRUE)
options(device = function(file, width = 8, height = 7, ...) {
  cairo_pdf(tempfile(), width = width, height = height, ...)
})
library("data.table")
library("ggplot2")
library("reshape2")
library("proto")
@ 



\end{document}
