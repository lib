%$Id: a0poster.cls,v 1.5 1999/04/05 14:49:56 mjf Exp $
%% 
%% This is file `a0poster.cls'
%% 
%% Copyright (C) 07.05.97 Gerlinde Kettl and Matthias Weiser
%%
%% Problems, bugs and comments to 
%% gerlinde.kettl@physik.uni-regensburg.de
%% 
%
%% This version changed by Hugh Pumphrey on 4.5.1999. Every instance
%% of the string ``draft'' replaced by the string ``preview''. This gives 
%% an a4 size preview but includes the postscript figures
%% Changed textwidths and margins to cope with printable area and
%% frame.  Make sure that the offsets are set to -1in!
%% Also changed scaling for a0->a4, since this was wrong.
%
%% Further modified by Ronald Kumon in June 2002 to adjust size 
%% of poster so that Acrobat Reader prints the entire poster 
%% on letter sized paper on my printer (it has strange margin offsets).
%% Also eliminated ``preview'' option because it does not work
%% and modified \thebibliography to output without a section heading.

\ProvidesClass{a0posterslides}[2002/06/07 v1.21b a0posterslides class 
(REK)]
\NeedsTeXFormat{LaTeX2e}[1995/06/01]
\LoadClass{article}

\newif\ifportrait
\newif\ifanullb
\newif\ifanull
\newif\ifaone
\newif\ifatwo
\newif\ifathree
\newif\ifletter
\newif\ifpreview
\newif\ifposter

\newcount\xcoord
\newcount\ycoord
\newcount\xscale
\newcount\yscale

\DeclareOption{a0b}{
\anullbtrue
\xcoord=2592 % big points (1 bp=1/72 inch)
\ycoord=3666 % big points (1 bp=1/72 inch)
\xscale=23
\yscale=23
}
\DeclareOption{a0}{
\anulltrue\anullbfalse
\xcoord=2380 % big points (1 bp=1/72 inch)
\ycoord=3368 % big points (1 bp=1/72 inch)
\xscale=25
\yscale=25
}
\DeclareOption{a1}{
\aonetrue\anullbfalse
\xcoord=1684 % big points (1 bp=1/72 inch)
\ycoord=2380 % big points (1 bp=1/72 inch)
\xscale=3
\yscale=3
}
\DeclareOption{a2}{
\atwotrue\anullbfalse
\xcoord=1190 % big points (1 bp=1/72 inch)
\ycoord=1684 % big points (1 bp=1/72 inch)
\xscale=4
\yscale=4

}
\DeclareOption{a3}{
\athreetrue\anullbfalse
\xcoord=842  % big points (1 bp=1/72 inch)
\ycoord=1190 % big points (1 bp=1/72 inch)
\xscale=6
\yscale=6
}
\DeclareOption{letter}{
\lettertrue\anullbfalse
\xcoord=612  % big points (1 bp=1/72 inch)
\ycoord=792 % big points (1 bp=1/72 inch)
\xscale=8
\yscale=8
}
\DeclareOption{landscape}{
\portraitfalse
}
\DeclareOption{portrait}{
\portraittrue
}
\DeclareOption{preview}{
\previewtrue
}
\DeclareOption{final}{
\previewfalse
}
\DeclareOption{poster}{
\postertrue
}
\DeclareOption{slides}{
\posterfalse
\anullfalse\anullbfalse\aonefalse\atwofalse\athreefalse
\portraittrue
\lettertrue
\xcoord=612  % big points (1 bp=1/72 inch)
\ycoord=792 % big points (1 bp=1/72 inch)
\xscale=8
\yscale=8
}
\DeclareOption*{\PackageWarning{a0poster}{Unknown Option \CurrentOption}}
\ExecuteOptions{landscape,a0b,poster,final}
\ProcessOptions\relax

\ifanullb
   \setlength{\paperwidth}{129.3cm} %% 36 * sqrt(2) in
   \setlength{\paperheight}{91.4cm} %% 36 in
   \setlength{\textwidth}{121.3cm} %% paperwidth - (4cm + 4cm)
   \setlength{\textheight}{83.4cm} %% paperheight - (4cm + 4cm)
\else\ifanull
%        \setlength{\paperwidth}{118.82cm}
%        \setlength{\paperheight}{83.96cm}
%        \setlength{\textwidth}{110.82cm} %% paperwidth - (4cm + 4cm)
%        \setlength{\textheight}{75.96cm} %% paperheight - (4cm + 4cm)
        \setlength{\paperwidth}{113.65cm}
        \setlength{\paperheight}{83.96cm}
        \setlength{\textwidth}{105.82cm} %% paperwidth - (4cm + 4cm)
        \setlength{\textheight}{75.96cm} %% paperheight - (4cm + 4cm)
     \else\ifaone
             \setlength{\paperwidth}{83.96cm}
             \setlength{\paperheight}{59.4cm}
             \setlength{\textwidth}{79.96cm}
             \setlength{\textheight}{55.4cm}
          \else\ifatwo
                  \setlength{\paperwidth}{59.4cm}
                  \setlength{\paperheight}{41.98cm}
                  \setlength{\textwidth}{55.4cm}
                  \setlength{\textheight}{37.98cm}
               \else\ifathree
                       \setlength{\paperwidth}{41.98cm}
                       \setlength{\paperheight}{29.7cm}
                       \setlength{\textwidth}{37.98cm}
                       \setlength{\textheight}{25.7cm}
                    \else\ifletter
                         \setlength{\paperwidth}{27.94cm}
                         \setlength{\paperheight}{21.59cm}
                         %\setlength{\textwidth}{25.4cm}
                         %\setlength{\textheight}{19.05cm}
                         \setlength{\textwidth}{22.86cm}
                         \setlength{\textheight}{16.51cm}
                         \else\relax
                         \fi
                    \fi
               \fi
          \fi
     \fi
\fi


\ifportrait
   \newdimen\exchange
   \setlength{\exchange}{\paperwidth}
   \setlength{\paperwidth}{\paperheight}
   \setlength{\paperheight}{\exchange}
   \setlength{\exchange}{\textwidth}
   \setlength{\textwidth}{\textheight}
   \setlength{\textheight}{\exchange}
\else\relax
\fi

%% Setting proper dimensions for the HP2500CP printer (height = 36 in)
%%   Landscape: unprintable areas
%%   	L: 27.6mm
%%   	R: 27.1mm
%%   	T: 18.4mm
%%   	B: 18.1mm


%\voffset -1in
%\hoffset -1in


\setlength{\headheight}{0 cm}
\setlength{\headsep}{0 cm}
\setlength{\topmargin}{0 cm} %% 3 cm for unprintable at top
			     %% (landscape) + 2 cm from border
\setlength{\topskip}{0 cm}
\ifposter
  %\setlength{\topmargin}{5 cm} %% 3 cm for unprintable at top
  %			     %% (landscape) + 2 cm from border
  %\setlength{\oddsidemargin}{5 cm} %% 3 cm for unprintable at left
  %			    	 %% (landscape) + 2 cm from border
  \setlength{\oddsidemargin}{0 cm} %% 3 cm for unprintable at left
  			    	 %% (landscape) + 2 cm from border
  \setlength{\evensidemargin}{0 cm} %% 3 cm for unprintable at left
  			    	 %% (landscape) + 2 cm from border
\else
%  \setlength{\oddsidemargin}{-1.252 cm} %% 0.5 in left margin
%  \setlength{\evensidemargin}{-1.252 cm} %% 0.5 in left margin
  \setlength{\oddsidemargin}{0 cm} %% 0.5 in left margin
  \setlength{\evensidemargin}{0 cm} %% 0.5 in left margin
\fi

% Removed this code because it does not work to do resizing!
%
%\catcode`\%=11
%\newwrite\HeaderFile
%\immediate\openout\HeaderFile=a0header.ps
%\write\HeaderFile{%%BeginFeature *PageSize ISOA0/ISO A0}
%\ifpreview\write\HeaderFile{2 dict dup /PageSize [595 842] put dup /ImagingBBox null put}
%\else\write\HeaderFile{2 dict dup /PageSize [\number\xcoord\space \number\ycoord] put dup /ImagingBBox null put}\fi
%\write\HeaderFile{setpagedevice}
%\ifpreview\write\HeaderFile{0.\number\xscale\space 0.\number\yscale\space scale}\else\relax\fi
%\write\HeaderFile{%%EndFeature}
%\closeout\HeaderFile
%\catcode`\%=14
%
%\special{header=./a0header.ps}

\input{a0size.sty}

\ifposter
  \renewcommand{\tiny}{\fontsize{12}{14}\selectfont}
  \renewcommand{\scriptsize}{\fontsize{14.4}{18}\selectfont}   
  \renewcommand{\footnotesize}{\fontsize{17.28}{22}\selectfont}
  \renewcommand{\small}{\fontsize{20.74}{25}\selectfont}
  \renewcommand{\normalsize}{\fontsize{24.88}{30}\selectfont}
  \renewcommand{\large}{\fontsize{29.86}{37}\selectfont}
  \renewcommand{\Large}{\fontsize{35.83}{45}\selectfont}
  \renewcommand{\LARGE}{\fontsize{43}{54}\selectfont}
  \renewcommand{\huge}{\fontsize{51.6}{64}\selectfont}
  \renewcommand{\Huge}{\fontsize{61.92}{77}\selectfont}
  \newcommand{\veryHuge}{\fontsize{74.3}{93}\selectfont}
  \newcommand{\VeryHuge}{\fontsize{89.16}{112}\selectfont}
  \newcommand{\VERYHuge}{\fontsize{107}{134}\selectfont}
\else
% borrowed from slides.cls
  \def\ifourteenpt{13.82}
  \def\iseventeenpt{16.59}
  \def\itwentypt{19.907}
  \def\itwentyfourpt{23.89}
  \def\itwentyninept{28.66}
  \def\ithirtyfourpt{34.4}
  \def\ifortyonept{41.28}
  \def\@setfontsize@parms#1#2#3#4#5#6#7#8{%
     \lineskip #1\relax%
     \parskip #2\relax
     \abovedisplayskip #3\relax
     \belowdisplayskip #4\relax
     \abovedisplayshortskip #5\relax
     \belowdisplayshortskip #6\relax
    \setbox\strutbox=\hbox{\vrule \@height#7\p@\@depth#8\p@\@width\z@}%
    \baselineskip\baselinestretch\baselineskip
    \normalbaselineskip\baselineskip}
  \DeclareMathSizes{13.82}{13.82}{10}{7}
  \DeclareMathSizes{16.59}{16.59}{12}{7}
  \DeclareMathSizes{19.907}{19.907}{16.59}{13.82}
  \DeclareMathSizes{23.89}{23.89}{19.907}{16.59}
  \DeclareMathSizes{28.66}{28.66}{23.89}{19.907}
  \DeclareMathSizes{34.4}{34.4}{28.66}{23.89}
  \DeclareMathSizes{41.28}{41.28}{34.4}{28.66}
  \def\normalsize{%
        \@setfontsize\normalsize\itwentypt{28\p@ plus3\p@ minus4\p@}%
        \@setfontsize@parms
              {2pt}%
              {30\p@ plus18\p@ minus9\p@}%
              {15\p@ plus3\p@ minus3\p@}%
              {10\p@ plus3\p@ minus3\p@}%
              {10\p@ plus3\p@}
              \abovedisplayshortskip
              {17}{7}}
  \normalsize
  \def\small{\@setfontsize\small\iseventeenpt{19\p@ plus3\p@ minus\p@}%
             \@setfontsize@parms
              {2\p@}%
              {15\p@ plus15\p@ minus7\p@}%
              {12\p@ plus3\p@ minus3\p@}%
              {9\p@ plus3\p@ minus3\p@}%
              {6\p@ plus3\p@}%
              \abovedisplayshortskip
              {13.5}{5.6}}
  \let\footnotesize=\small
  \let\scriptsize=\small
  \def\tiny{\@setfontsize\tiny\ifourteenpt{16\p@ plus2\p@ minus\p@}%
            \@setfontsize@parms
              {2pt}%
              {14\p@ plus3\p@ minus10\p@}%
              {11\p@ plus3\p@ minus10\p@}%
              \abovedisplayskip
              {8\p@ plus3\p@ minus5\p@}%
              {\z@ plus3\p@}%
              {10}{4}}
  \def\large{\@setfontsize\large\itwentyfourpt{42\p@ plus8\p@ minus5\p@}%
             \@setfontsize@parms
              {2\p@}%
              {40\p@ plus20\p@ minus4\p@}%
              {20\p@ plus8\p@ minus3\p@}%
              \abovedisplayskip
              {10\p@ plus5\p@}%
              \abovedisplayshortskip
              {20}{8.5}}
  \def\Large{\@setfontsize\Large\itwentyninept{48\p@ plus10\p@ minus6\p@}%
             \@setfontsize@parms
              {2\p@}%
              {48\p@ plus30\p@ minus6\p@}%
              {24\p@ plus10\p@ minus6\p@}%
              \abovedisplayskip
              {12\p@ plus8\p@}%
              \abovedisplayshortskip
              {27}{11}}
  \def\LARGE{\@setfontsize\LARGE\ithirtyfourpt{52\p@ plus10\p@ minus6\p@}%
             \@setfontsize@parms
              {2\p@}%
              {52\p@ plus30\p@ minus6\p@}%
              {24\p@ plus10\p@ minus6\p@}%
              \abovedisplayskip
              {12\p@ plus8\p@}%
              \abovedisplayshortskip
              {27}{11}}
  \def\huge{\@setfontsize\huge\ifortyonept{60\p@ plus10\p@ minus6\p@}%
            \@setfontsize@parms
              {2\p@}%
              {60\p@ plus30\p@ minus6\p@}%
              {24\p@ plus10\p@ minus6\p@}%
              \abovedisplayskip
              {12\p@ plus8\p@}%
              \abovedisplayshortskip
              {27}{11}}
  \let\Huge\huge
  \let\veryHuge\huge
  \let\VeryHuge\huge
  \let\VERYHuge\huge
\fi

\setlength\smallskipamount{6pt plus 2pt minus 2pt}
\setlength\medskipamount{12pt plus 4pt minus 4pt}
\setlength\bigskipamount{24pt plus 8pt minus 8pt}

\setlength\abovecaptionskip{25pt}
\setlength\belowcaptionskip{0pt}
\setlength\abovedisplayskip{25pt plus 6pt minus 15 pt}
\setlength\abovedisplayshortskip{0pt plus 6pt}
\setlength\belowdisplayshortskip{13pt plus 7pt minus 6pt}
\setlength\belowdisplayskip\abovedisplayskip

\normalsize

\AtBeginDocument{
\ifanullb
   \ifportrait\special{papersize=91.4cm,129.3cm}\else\special{papersize=129.3cm,91.4cm}\fi
\else\ifanull
        \ifportrait\special{papersize=83.96cm,118.82cm}\else\special{papersize=118.82cm,83.96cm}\fi
     \else\ifaone
             \ifportrait\special{papersize=59.4cm,83.96cm}\else\special{papersize=83.96cm,59.4cm}\fi
          \else\ifatwo
                  \ifportrait\special{papersize=41.98cm,59.4cm}\else\special{papersize=59.4cm,41.98cm}\fi
               \else\ifathree
                      \ifpreview
                       \ifportrait\special{papersize=29.7cm,41.35cm}\else\special{papersize=41.98cm,29.0cm}\fi
                      \else
                       \ifportrait\special{papersize=29.7cm,41.98cm}\else\special{papersize=41.98cm,29.7cm}\fi
                      \fi
                        \else\ifletter
                           \ifportrait\special{papersize=21.59cm,27.94cm}\else\special{papersize=27.94cm,21.59cm}\fi
                            \else\relax
                        \fi
                    \fi
               \fi
          \fi
     \fi
\fi
\pagestyle{empty}}

% Redefine \thebibliography environment so that it does not print a heading
%
%\def\thebibliography#1{\setlength{\labelsep}{0.0pt}\list
%{[\arabic{enumi}]}{\settowidth\labelwidth{[#1]}
%\leftmargin\labelwidth
% \advance\leftmargin\labelsep
% \usecounter{enumi}}
% \def\newblock{\hskip .11em plus .33em minus -.07em}
% \sloppy
% \sfcode`\.=1000\relax}
%\let\endthebibliography=\endlist

\endinput
%% 
%% End of file `a0poster.cls'.
