# -*- mode: snippet -*-
# name: debbugs_modify
# key: debbugs_modify
# --

As a rule we do not modify bug reports that have been submitted to the
BTS. This includes deleting content, masking e-mails or otherwise
hiding information that has been submitted to the BTS.

The only modifications that we do make are removing messages which are
obviously spam.

If you are concerned about your e-mail address being made public by
continuing to be present on bugs.debian.org, it is already too late.
Every message that is sent to the BTS is broadcast to a number of
mailing lists and mirrored throughout the internet. Spammers are known
to be subscribed to these mailing lists.

Instead, we strongly suggest that you invest in anti spam software
like spamassassin or similar, as removing or otherwise munging
information will not stem the tide of spam at all.

Please see http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=63995 et
al. if you have more questions about this.
