; This stub's entire point is to install use-package and org mode so
; everything else can be made

;; add /etc/ssl/ca-global/ca-certificates.crt so that we can download
;; packages when we're on Debian hosts which chop down the list of
;; available certificates
(require 'gnutls)
(add-to-list 'gnutls-trustfiles "/etc/ssl/ca-global/ca-certificates.crt")
(setq package-enable-at-startup nil)
(setq package--init-file-ensured t)
(setq package-user-dir "~/var/emacs/elpa")
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org" . "http://orgmode.org/elpa/") ))

(defvar bootstrap-version)
(setq straight-base-dir '"~/var/emacs/")
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" straight-base-dir))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(setq use-package-verbose (not (bound-and-true-p byte-compile-current-file)))
(use-package org
  )
