SSCONVERT ?=ssconvert

%.xls: %.gnumeric
	$(SSCONVERT) --export-type Gnumeric_Excel:excel_dsf $< $@

%.txt: %.gnumeric
	$(SSCONVERT) --export-type Gnumeric_stf:stf_assistant -O 'separator="	" quote="" eol="unix"' $< $@

%.txt: %.xls
	$(SSCONVERT) --export-type Gnumeric_stf:stf_assistant -O 'separator="	" quote="" eol="unix"' $< $@

%.txt: %.xlsx
	$(SSCONVERT) --export-type Gnumeric_stf:stf_assistant -O 'separator="	" quote="" eol="unix"' $< $@

%.xls: %.txt
	txt2xls $< > $@
